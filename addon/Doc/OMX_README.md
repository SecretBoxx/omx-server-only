## **OMX**
OMX v1.1.0, GPLv2, issued 2024-02-06

OMX is a fork and a continuation of the great OMP by Friedrich Kastner-Masilko (in GPLv2) to be used with Orbiter 2016 created by Dr. Martin Scheiger (in MIT license).

OMX is the multi-user engine, under GPLv2, prepared for our own gameplay Nex'Orbiter, a multi-user persistent universe to travel "at 1G" and explore the solar system, its worlds, its distances, in the respect of the laws of physics and physiology. OMX is in GPLv2. Nex'Orbiter is not open-source, info & contact here: https://nexorbiter.com/

As OMP is an open-source project under GNU GPL Version 2, all its modifications must remain open-source under GNU GPL Version 2:
- The GPL v2 is provided here as a text file.
- The source code of OMP, as forked, is available as the initial commit of the project https://gitlab.com/SecretBoxx/omx 
- The source codes of the modified versions (labelled OMX) are available at the same URL, usually tagged whenever a new external contribution or a new release was added.

2022-2024, Boris Segret, PhD, a.k.a. Boxx @ orbiter-forum.com

Powered by OMX v.1.1.0:
+============ Nex'Orbiter =============+
|==== Dura Lex Gravitate, Sed Lex Gravitate ====|
+========== the Global Brain ==========+

## Installation
WARNING:
- the installation requires Orbiter 2016, with its Orbitersdk subfolder, as well as (at the moment) Visual Studio 2019 (no 2022+) with C++ Desktop and .NET Desktop additions, because the runtime does not work properly.
- Warning, a modified "VESSEL.h" will be installed separately in ..\..\include, as well as some textures that replace previous ones.

Step-by-step:
1. Install, if not already, MS Visual Studio 2019 (e.g. Community) with C++ and .NET for desktops.
2. Re-install a vanilla version of Orbiter 2016 as a new folder (you can have multiple installs).
3. Unzip the OMX zip file into Orbiter's root, and accept all replacement.
4. Run Orbiter.exe
5. In the launchpad, uncheck the "Limited fuel" option in the Parameters
6. In the launchpad, select "OMPClient" in the Modules
7. In OMP Client window, Network Tab:
	- setup your server as orbiterx.obspm.fr with port 1515
	- select a name (vessel name at the moment) and keep "toto" as a password (experimental)
8. In the launchpad, click on "Connect OrbiterX" and follow the process.

See "known issues" in OMX_Features.md in case of difficulties or contact on Orbiter-forum.com.

The installation package provides a zip file to be unzipped into a vanilla installation of Orbiter 2016. For convenience, this package contains the minimum required for OMX, i.e. it includes also 3rd parties not part of OMX repository (scenarii, bases...) for a ready-to-use experience.

Make sure you first go back to this minimum install and deselect any non requested addons, before reporting a bug, as no compatibility with other addons are guaranted. Orbiter_ng (D3D9) and XRSound were NOT tested with OMX. Nevertheless, OrbiterSound 5.0 (3D) Module and High resolution textures were tested compatible with OMX.

## Support
Go to Orbiter-Forum for support:
- you may surch for Boxx' threads and addons.
- a dedicated subforum for OMP exists.
Go to Discord > Orbiter-forum server > OMP subgroup for live support (if somebody's there)
Go to Twitch for live streaming https://www.twitch.tv/orbinautboxx
Go to Nex'Orbiter website to discover what YOU could do https://nexorbiter.com/

## Roadmap

- (done in v1.1.0) The very first priority was to transfer vessels' status from a Client to another.
- Doc/OMX_Features.md lists the additions since OMX v.1.0.0 and the known limitations.
- OMX_CICD.txt at project's root summarizes the tests performed before releasing.
- OMX_ToDoList.txt at project's root presents a series of features highly desirable. Get inspired!

## Contributing
OMX is a fork of Face's OMP project 0.8.2, open source:
- here https://hg.osdn.net/view/orbitersoftware/OMP/archive/0.8.2.zip 
- or at the root (2022-09-17) of OMX' repository here https://gitlab.com/SecretBoxx/omx 

Parking Brake, by asbjos @ orbiter-forum.com is here:
- here https://www.orbiter-forum.com/resources/parking-brake.3105/ 
- or at the tag ParkingBrake (2023-12-17) of OMX' repository (commit b94c6d531695760de79d1cb89f4575282225188f)

ACKNOWLEGEMENT: for information, the current implementation (since 2022) of Nex'Orbiter is a non-commercial use and acknowledges the following contributions by the community. May they be thanked. Nex'Orbiter makes use of OMX which is only a part of Nex'Orbiter.
- ORBITER 2016, by Dr. Martin Schweiger, open source under MIT license
- OrbiterSound 5.0, by DanSteph et al., freeware, http://orbiter.dansteph.com/
- D3D9 Client 4.25, by Jarmonik, freeware, http://users.kymp.net/~p501474a/D3D9Client/ 
- HiRes textures, by Dr. Martin Schweiger
- OMP 0.8.2, by Friedrich Kastner-Masilko, open source under GPL v2 license, as modified into OMX
- Rochambeau 12/2020, by Papyref et al., freeware (tbc)
- ORL 1.1, by Mohd "computerex" Ali, Artyom "Artlav" Litvinovich, Tom "wehaveaproblem" Fisher, Ben "TSPenguin" Stickan, "RacerX", freeware (tbc)
- Parking Brake, by asbjos @ orbiter-forum.com, GPLv2 granted to OMX on 2023-12-17
