## **OMX**
OMX v1.1.0, GPLv2, issued 2024-02-06

Powered by OMX v.1.1.0:

+============ Nex'Orbiter =============+
|=== Dura Lex Gravitate, Sed Lex Gravitate ===|
+========== the Global Brain ==========+
Contact Nex'Orbiter for any suggestions: https://nexorbiter.com
Report any bugs on OMX Addon's page @ orbiter-forum.com

## New features

OMX v1.1.0:
- MFD STC shows if a vessel is locally controlled and if it is grounded (landed)
- Shorter GINFO sent (extended only in "transfer")
- Landed status reviewed and extended (inspired by ParkingBrake)
- More stability in vessel transfers, keeping thrust or parking status
- (fix) vessel status on non-spherical surfaces managed locally and shared remotely
- (fix) remote vessel suspended and/or jittering above surface
- (fix) while transfering, prevented the ghost from tumbling like crazy
- (for devs) Review of the Critical Sections calls, with Logging at level 5
- Note: user account management is implemented but not deployed yet.
Special Thanks for this version: jacquesmomo, asbjos

OMX v1.0.0:
- New Space Traffic Control MFD added (with poor GUI, ok)
- Offer/Take vessel was made functional, for alpha debug
- scenarii at Rochambeau airport and in deep Space (Mars, Phobos)
Special Thanks for this version: Face

## Known issues

OMX makes Orbiter crash when selecting the module OMPClient:
- either you need Visual Studio 2019 (no 2022+) with C++ and .NET
- or you've got a "locked" file due to over-security by Windows. See in particular all .dll files after unzipping with right-click > Properties.

A landed remote vessel is seen trembling somehow. No impact, the vessel is correctly landed on its host client.

Client version N1 (as seen in Client's window banner) is said incompatible with server version N2, which is earlier. No back-compatibility. Use the latest version only.

When a vessel is locally controled and has the focus, the focus is lost whenever it is taken. Instead the focus should come back to that vessel after transfer. It is important for Flying Schools. Indeed, to be fixed.

## Offer/Accept, Take/Give in STC MFD

The offer/accept process is a key feature in OMX. Please report any bug but make sure you worked on a vanilla install of Orbiter + OMX v1.1.0 Zip file only. Make sure that you have Visual Studio 2019 with C++ and .NET (see issue) and no "locked" file after unzipping (see issue).

Summary: a local vessel can be offered to a remote STC before disconnecting. See the summary of the processes to offer the local vessel or to take back a remote vessel in OMX' manual.

FIO. Process related to additions in the server:
- Server, Host.cs (case CommandLineRules.Command_GINFO) \ eval = vessel.EvaluateInfo
- Server, Vessel.cs, EvaluateInfo
