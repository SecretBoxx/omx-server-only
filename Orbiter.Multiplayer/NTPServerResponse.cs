namespace Orbiter.Multiplayer
{
    /// <summary>
    /// NTP server response structure.
    /// </summary>
    public class NTPServerResponse
    {
        private int leapIndicator;
        private int stratum;
        private NTPTime originateTime;
        private NTPTime receiveTime;
        private NTPTime transmitTime;
        private NTPTime destinationTime;
        private double roundTripDelay;
        private double localClockOffset;

        /// <summary>
        /// Returns the leap indicator value.
        /// </summary>
        /// <remarks>
        /// The value has the following meaning:
        /// <list type="bullet">
        /// <item>0: no warning</item>
        /// <item>1: last minute in day has 61 seconds</item>
        /// <item>2: last minute has 59 seconds</item>
        /// <item>3: clock not synchronized</item>
        /// </list>
        /// </remarks>
        public int LeapIndicator
        {
            get { return leapIndicator; }
        }

        /// <summary>
        /// Returns the stratum value.
        /// </summary>
        /// <remarks>
        /// The value has the following meaning:
        /// <list type="bullet">
        /// <item>0: unspecified or unavailable</item>
        /// <item>1: primary reference (e.g., radio clock)</item>
        /// <item>2-15: secondary reference (via NTP or SNTP)</item>
        /// <item>16-255: reserved</item>
        /// </list>
        /// </remarks>
        public int Stratum
        {
            get { return stratum; }
        }

        /// <summary>
        /// Returns the time when the request was sent from the client to the SNTP server.
        /// </summary>
        public NTPTime OriginateTime
        {
            get { return originateTime; }
        }

        /// <summary>
        /// Returns the time when the request was received by the server.
        /// </summary>
        public NTPTime ReceiveTime
        {
            get { return receiveTime; }
        }

        /// <summary>
        /// Returns the time when the server sent the request back to the client.
        /// </summary>
        public NTPTime TransmitTime
        {
            get { return transmitTime; }
        }

        /// <summary>
        /// Returns the time when the reply was received by the client.
        /// </summary>
        public NTPTime DestinationTime
        {
            get { return destinationTime; }
        }

        /// <summary>
        /// Returns the round trip time in seconds.
        /// </summary>
        public double RoundTripDelay
        {
            get { return roundTripDelay; }
        }

        /// <summary>
        /// Returns the local clock offset relative to the server.
        /// </summary>
        public double LocalClockOffset
        {
            get { return localClockOffset; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NTPServerResponse"/> class.
        /// </summary>
        /// <param name="leapIndicator">The leap indicator.</param>
        /// <param name="stratum">The stratum.</param>
        /// <param name="times">The times.</param>
        /// <param name="roundTripDelay">The round trip delay.</param>
        /// <param name="localClockOffset">The local clock offset.</param>
        public NTPServerResponse(int leapIndicator, int stratum, NTPTime[] times, double roundTripDelay, double localClockOffset)
        {
            this.leapIndicator = leapIndicator;
            this.stratum = stratum;
            if (times != null)
            {
                if (times.Length > 0) originateTime = times[0];
                if (times.Length > 1) receiveTime = times[1];
                if (times.Length > 2) transmitTime = times[2];
                if (times.Length > 3) destinationTime = times[3];
            }
            this.roundTripDelay = roundTripDelay;
            this.localClockOffset = localClockOffset;
        }
    };
}
