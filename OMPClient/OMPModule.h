#pragma once

#ifndef OMPMODULE_H
#define OMPMODULE_H

#include "orbitersdk.h"
//#include "HashTable.h"


			/*struct MessageLoad
			{
				double				timestamp;
				double				distance;
				double				frequency;
				OBJHANDLE			object;
				char* message;
				struct MessageLoad* next;
			};*/

			/*struct KillLoad
			{
				OBJHANDLE			object;
				struct KillLoad* next;
			};*/

			/*struct payload
			{
				char* nick, * passwd;
				unsigned long ip_addr;
				int receiverport, sendsocket, sendtoport;
				double timestamp;
				//Hashtables
				CHashTable* GlobalsByID;
				CHashTable* GlobalsByHandle;
				CHashTable* LocalsByID;
				unsigned short int LocalID;
				CHashTable* Neighbours;
				struct MessageLoad* MessageQueue;
				struct KillLoad* KillList;
			};*/

			/*struct IDload
			{
				unsigned short int	ID;
				unsigned short int	refcount;
				int					MasterID;
				IDload* GlobalID;
				OBJHANDLE			object;
				VESSEL* vessel;
				char				size;
				char				linkage;	// related to the user's vessel (1? 2?)
				double				tsSent;
				char* NavInfo;
				char* DockInfo;
				union
				{
					//for local vessels
					struct
					{
						unsigned short int	MasterPreset;
						unsigned char		justAdded;
						int					idle;
						double				lastJump;
					};
					//for remote vessels
					struct
					{
						THRUSTER_HANDLE		thruster;
						PROPELLANT_HANDLE	fuel;
						double				lastInfoPing;
					};
				};
			};*/

			/*struct
			{
				IDload* id;
				char* cls;
				unsigned short		local;
			} MoveLoad;*/

			//CRITICAL_SECTION hashing;
			//HANDLE startmove;
			//HANDLE stoppedmove;
//#define THREADS			6			//Status, TCP-RCVR, UDP-XMTR, UDP-RCVR, SYNC, GC
			/*struct Sthreaddata
			{
				int com, thisport;
				int sin_size;// = sizeof(struct sockaddr);
				struct sockaddr_in remote_addr;
				HANDLE hThread;
				DWORD ThreadID;
				bool activation;
				payload* user;
				CRITICAL_SECTION mutex;
			};*/
			//struct Sthreaddata* threaddata[THREADS], * xthreaddata, * rthreaddata;

class OMPModule : public oapi::Module
{
public:
	OMPModule(HINSTANCE hDLL);
	~OMPModule(void);
	//virtual void clbkSimulationStart (RenderMode mode);
	virtual void clbkSimulationEnd ();
	virtual void clbkPreStep (double simt, double simdt, double mjd);
	virtual void clbkPostStep (double simt, double simdt, double mjd);
	virtual void clbkNewVessel(OBJHANDLE hVessel);
	virtual void clbkDeleteVessel(OBJHANDLE hVessel);
private:
	HINSTANCE hDLL;
};

#endif // OMPMODULE_H