// ==============================================================
//              "Space Traffic Control" MFD for OMX
// part of STC
//   (c) 11/2022-2024+ Boris Segret, due to OMP, GPL license applies
// included:
//   bool STC_OfferVessels(double simTime)
//   bool STC_TakbackVessels(double simTime)
//   void STC_OwnVesselLanded(OBJHANDLE handle)
//   void STC_LandIt(VESSEL* v, OBJHANDLE rbody, double longitude, double latitude, double heading)
// ============================================================================

/// <summary>
/// Called by OMPClient in PreStep(...) if STC_offergInProcess is true: the next remaining
/// local vessel will be offered (if STC_ALLtrigger) or only the [offrdName]. It turns 
/// on & off various STC_... global flags, initizes STC_.. global properties.
/// </summary>
/// <param name="simTime">current sim time for watchdog</param>
bool STC_OfferVessels(double simTime)
{
	char cmd[256], line[256];
	short int remainingNow;
	struct IDload* entry;
	remainingNow = clientData.LocalsByID->get_count();
	sprintf(line, "STC_OfferVessels: %d local vessels", remainingNow);
	if (remainingNow == 0) {
		STC_offergInProcess = false;
		STC_clearToOffer = true;
		STC_globalId = 0;
		STC_offergWatchdog = 0.;
		sprintf(STC_offrdName, "NN-iii");
		return true;
	}

	sprintf(line, "%s, is \"%s\" in transfer to %s?", line, STC_offrdName, hostgName);
	if (!STC_clearToOffer) {
		// a previous OFFER is still in progress
		if (simTime > STC_offergWatchdog + 10) {
			// but the OFFER process lasts too long (likely NO accept) => abort!
			sprintf(cmd, "offer off %d", STC_globalId);
			sprintf(line, "%s --> %s", line, cmd);
			sprintf(cmd, "%s\n\r", cmd); // adding the EOL
EnterCriticalSection(&(xthreaddata->mutex));
			if (send(xthreaddata->com, cmd, strlen(cmd), 0) == SOCKET_ERROR)
			{
				sprintf(line, "%s, Error sending to %s:%d", line, dinfo.srvipchar, ntohs(xthreaddata->remote_addr.sin_port));
				tlogging(20, line);
			}
LeaveCriticalSection(&(xthreaddata->mutex));
			sprintf(line, "%s, aborted by Watchdog.", line);
			tlogging(20, line);
			STC_offergInProcess = false;
			STC_clearToOffer = true;
			STC_globalId = 0;
			return true;
		}
		else {
			// when vessel is accepted by B, it is killed by A (killList on GINFO <va>"A)
			// the watchdog runs as long as the local vessel is still in Local list
			// the GlobalsByHandle and LocalsByID lists are updated in DeleteVessel
			if (STC_isVesselLocal(STC_offrdName)) {
				sprintf(line, "%s yes -> return.", line); tlogging(5, line);
				return true;
			}
			else {
				// end of the current offer process, as seen from Client A
				sprintf(line, "%s. Was transferred", line);
			}
		}
	}

	int nbLocals = clientData.LocalsByID->get_count();
	int nbLocal  = 0, ii;
	for (ii = 0; nbLocal < nbLocals; ii++)
	{
		sprintf(cmd, "%d", ii);
		if ((entry = (struct IDload*)clientData.LocalsByID->get(cmd)) != NULL)
		{ // if no local index, then bypass and check the next one in descending order
			// we found a local vessel:
			sprintf(cmd, "%s", entry->vessel->GetName());
			// if ALL any local vessel must be offered
			if (STC_ALLtrigger) break;
			else if (strcmp(cmd, STC_offrdName) == 0) break; // if not ALL, check if this one is the offered one
			nbLocal++;
		}
	}
	if (nbLocal == nbLocals) {
		// no local vessel matches
		STC_offergInProcess = false;
		STC_clearToOffer = true;
		STC_offergWatchdog = 0.;
		STC_globalId = 0;
		sprintf(line, "%s No local vessel -> return.", line); tlogging(5, line);
		return true;
	}
	// (TODO) also check for a valid STC
	STC_globalId = entry->GlobalID->ID;
	entry = entry->GlobalID;
	STC_clearToOffer = false;
	STC_offergWatchdog = simTime;
	sprintf(line, "%s Now, yes: [L%d]", line, ii);
	tlogging(5, line);

	STC_UpdateState(entry);
	// this update will be used by Client B later, now just stored by the server (not broadcasted)

	sprintf(cmd, "offer %d %s", STC_globalId, hostgName);
	sprintf(line, "%s --> %s", line, cmd);
	sprintf(cmd, "%s\n\r", cmd); // adding the EOL
	//tlogging(5, line);
	EnterCriticalSection(&(xthreaddata->mutex));
	if (send(xthreaddata->com, cmd, strlen(cmd), 0) == SOCKET_ERROR)
	{
		sprintf(line, "%s, Error sending to %s:%d", line, dinfo.srvipchar, ntohs(xthreaddata->remote_addr.sin_port));
		tlogging(20, line);
	}
	LeaveCriticalSection(&(xthreaddata->mutex));
	sprintf(line, "%s, done!", line);
	tlogging(5, line);
	return true;
}

/// <summary>
/// Commands to the server to get back a vessel. If successfull, the process ends with the focus in the cockpit. Returns FALSE as long as a Taking-back Vessel in in process.
/// </summary>
/// <param name="simTime">current sim time for wtachdog</param>
/// <returns>TRUE as soon as the process is over.</returns>
bool STC_TakbackVessels(double simTime)
{
	char cmd[256], line[256], debugMsg[LINESIZE];
	short int remainingNow;

	sprintf(debugMsg, "STC_TakbackVessels:");

	// the requested vessel is <takenName>, this routine is called to take back user's 
	// own vessel (if registered) or the vessel on focus if non-registered (server's check).
	if (!STC_givenBack) {
		// a previous GVBCK is still in progress
		if ((STC_takgIsLocal = STC_isVesselLocal(STC_takenName))) {
				// checks if the requested vessel is back, i.e. local
				struct IDload* entry;
				sprintf(debugMsg, "%s %s=[G%d] was returned", debugMsg, STC_takenName, STC_globalId);
				sprintf(line, "%d", STC_globalId);
				if ((entry = (struct IDload*)clientData.GlobalsByID->get(line)) == NULL) {
					sprintf(debugMsg, "%s but NOT recreated!!", debugMsg);
					tlogging(20, debugMsg);
				} else if (entry->object != NULL) {
					oapiSetFocusObject(entry->object);
					sprintf(debugMsg, "%s and focused on vH%p", debugMsg, entry->object);
					tlogging(5, debugMsg);
				}
				STC_takingBack = false;
				STC_clearToOffer = true; // redondant
				STC_clearToAccpt = true; // redondant
				STC_globalId = 0;
				STC_givenBack = true;
		} else if (simTime > STC_takgWatchdog + 10) {
				// but the OFFER process lasts too long (likely NO accept) => abort!
				sprintf(cmd, "offer off %d\n\r", STC_globalId);
				sprintf(debugMsg, "%s --> %s", debugMsg, cmd);
				tlogging(5, debugMsg);
				EnterCriticalSection(&(xthreaddata->mutex));
				if (send(xthreaddata->com, cmd, strlen(cmd), 0) == SOCKET_ERROR)
				{
					sprintf(debugMsg, "%s, Error sending to %s:%d...", debugMsg, dinfo.srvipchar, ntohs(xthreaddata->remote_addr.sin_port));
					tlogging(20, debugMsg);
				}
				LeaveCriticalSection(&(xthreaddata->mutex));
				sprintf(debugMsg, "%s aborted by Watchdog, vessel G%d", debugMsg, STC_globalId);
				tlogging(20, debugMsg);
				STC_takingBack = false;
				STC_clearToOffer = true;
				STC_clearToAccpt = true;
				STC_globalId = 0;
				STC_givenBack = true;
				return false;
		} // else sprintf(debugMsg, "%s but %s [G%d] still not recreated", debugMsg, STC_takenName, STC_globalId);
		// tlogging(5, debugMsg);
		return true;
	}

	// TkBck process to be started
	sprintf(cmd, "tkbck %s", STC_takenName);
	sprintf(debugMsg, "%s --> %s", debugMsg, cmd);
	sprintf(cmd, "%s\n\r", cmd);
	EnterCriticalSection(&(xthreaddata->mutex));
	if (send(xthreaddata->com, cmd, strlen(cmd), 0) == SOCKET_ERROR)
	{
		sprintf(line, "%s, Error sending to %s:%d", debugMsg, dinfo.srvipchar, ntohs(xthreaddata->remote_addr.sin_port));
		tlogging(20, line);
	}
	LeaveCriticalSection(&(xthreaddata->mutex));
	STC_givenBack = false;
	STC_takgWatchdog = simTime;
	sprintf(debugMsg, "%s, sent!", debugMsg);
	tlogging(5, debugMsg);
	return true;
}

/// <summary>
/// Forces the handle's vessel to be landed, only if local vessel
/// </summary>
/// <param name="handle">must be a vessel handle</param>
void STC_OwnVesselLanded(OBJHANDLE vHandle)
{
	// here, we adapt Parking Brake MFD code to a permanent "Near-0 velocity mode"
	// => see STC_LandIt(...) for credits
	OBJHANDLE ref;
	IDload* entry;
	IDload* pBody;
	double longitude, latitude, radius;
	double heading = 0.0;
	VESSELSTATUS2 vs;
	memset(&vs, 0, sizeof(vs));
	char line[LINESIZE],logg[100];

	//sprintf(line, "STC_OwnVesselLanded > in, vH%p", vHandle); tlogging(5, line);
	VESSEL* v = oapiGetVesselInterface(vHandle);
	sprintf(line, "%p", vHandle);
	if ((entry = (struct IDload*)clientData.GlobalsByHandle->get(line)) == NULL) {
		sprintf(line, "STC_OwnVesselLanded, entry %s still not referenced => return", v->GetName());
		tlogging(5, line);
		return;
	}

	// Only do extensive check if not already landed, but in contact with planet.
	if (v->GroundContact()) // ground contact, maybe not still.
	{
		if (v->GetThrusterGroupLevel(THGROUP_MAIN) < 0.01 && v->GetThrusterGroupLevel(THGROUP_HOVER) < 0.01 && v->GetThrusterGroupLevel(THGROUP_RETRO) < 0.01)
			// main thrusters near-to-Zero (RCS allowed, linear or rotation)
		{
			if (v->GetGroundspeed() < 0.5)
				// speed lower than 0.5m/s (docking requests 0.1m/s)
			{
				VECTOR3 vv = _V(0, 0, 0);
				v->GetAngularVel(vv);
				if (length(vv) < 30 * PI / 180)
					// low-tumbling (<30deg/s, i.e. 30*pi/180 rad/s)
				{
					if (v->GetPitch() < PI / 6 && v->GetBank() < PI / 6)
						// the orientation must be close to horizontal (30deg max, we don't care the yaw angle)
					{
						if (!entry->Landed) {
							// => force Landed status + notify the server
							//tlogging(5, "STC_OwnVesselLanded > do we LandIt?");
							ref = v->GetSurfaceRef();
							v->GetEquPos(longitude, latitude, radius);
							oapiGetHeading(vHandle, &heading);
							STC_LandIt(v, ref, longitude, latitude, heading);
							//tlogging(5, "STC_OwnVesselLanded > STC_LandIt: back!");

							sprintf(line, "%p", ref);
							pBody = (struct IDload*)clientData.GlobalsByHandle->get(line);
							entry->Landed = true;
							entry->LandBody = pBody->ID;
							entry->LandInfo[0] = longitude;
							entry->LandInfo[1] = latitude;
							entry->LandInfo[2] = heading;
							STC_UpdateState(entry);
							sprintf(line, "STC_OwnVesselLanded > %s [G%d] STATUS Landed > update with %5d %12.9f %12.9f %12.9f", v->GetName(), entry->ID, pBody->ID, longitude, latitude, heading);
							tlogging(5, line);
						} // if already "Landed", does nothing but returns
						return;
					}
				}
			}
		}
	}
	// no "Landed" conditions (maybe on surface but not still)
	// if Status was Landed, then updates + notifies the server
	if (entry->Landed) {
		entry->Landed = false;
		sprintf(line, "STC_OwnVesselLanded > %s [G%d] STATUS Flying > update", v->GetName(), entry->ID);
		tlogging(5, line);
		STC_UpdateState(entry);
	}
	//tlogging(5, "OMX: STC_OwnVesselLanded (out)");
	return;
}

void STC_LandIt(VESSEL* v, OBJHANDLE rbody, double longitude, double latitude, double heading)
{
	// Thanks to "asbjos" (under GPLv2 for OMX): https://www.orbiter-forum.com/resources/parking-brake.3105/ 
	// Thanks to "jarmonik": https://www.orbiter-forum.com/threads/mars-2020-rover-and-mars-helicopter-scout.37153/page-6#post-577597
	// Also honorably mention to "Face": https://www.orbiter-forum.com/threads/orbiter-economic-simulation.39629/page-2#post-578205

	VESSELSTATUS2 vs;
	char line[LINESIZE];

	//sprintf(line, "STC_LandIt > vessel? %s", v->GetName()); tlogging(5, line);

	// we set gears down to secure interaction with surface (actually should be checked from animations, but
	// anyway, altitude and gears status are controlled by other TCP/UDP messages)
	v->SetTouchdownPoints(tdvtx_geardown, ntdvtx_geardown);
	vs.version = 2;
	vs.flag = 0x0;
	v->GetStatusEx(&vs);
	//sprintf(line, "STC_LandIt > rbody %p, lg %f, lat %f, hg %f", rbody, longitude, latitude, heading); tlogging(5, line);
	vs.rbody = rbody;
	vs.status = 1; // Landed
	vs.rvel = _V(0, 0, 0);
	vs.arot = _V(0, 0, 0);
	vs.vrot = _V(0, 0, 0);
	vs.arot.x = 10; // <----- Undocumented feature "magic value" to land on touchdown points !! IMPORTANT !! It has to be 10, no more, no less !
	vs.surf_lng = longitude;
	vs.surf_lat = latitude;
	vs.surf_hdg = heading;
	// in order to "smooth" the anchoring on a small body surface and align with the local mesh, maybe consider:
	// VECTOR3 VESSEL::GetSurfaceNormal  (  ) const 
	// ELEVHANDLE oapiElevationManager  ( OBJHANDLE  hPlanet ), then an access to the tiles would be needed (bool oapi::GraphicsClient::ElevationGrid?)
	// double oapiSurfaceElevation  ( OBJHANDLE  hPlanet,  double  lng,	double  lat	)
	// (or) double VESSEL::GetSurfaceElevation  (  ) const 
	// BOOL oapiGetAltitude  ( OBJHANDLE  hVessel,  AltitudeMode  mode, double* alt)
	// + cancel totally all rotation, thrusts and auto-modes
	v->DefSetStateEx(&vs);
	return;
}
