// ==============================================================
//              "Space Traffic Control" MFD for OMX
// part of STC
//   (c) 11/2022-2024+ Boris Segret, due to OMP, GPL license applies
// included:
//   bool STC_isVesselLocal(char* vesselName)
//   bool STC_isVesselLocal(OBJHANDLE hVessel)
//   bool STC_isVesselLocal(int gID)
//   void STC_UpdateState(IDload* entry)
//   void STC_RestoreState(IDload* entry, bool deleteAfter)
// ==============================================================

bool STC_isVesselLocal(char* vesselName)
{
	OBJHANDLE hVessel;
	char msg[100];

	if ((hVessel = oapiGetVesselByName(vesselName)) == NULL) return false;
	return STC_isVesselLocal(hVessel);
}

bool STC_isVesselLocal(OBJHANDLE hVessel)
{
	IDload* entry = NULL;
	CHashData *entryHash = NULL;
	char line[LINESIZE], msg[LINESIZE];
	int nbOfLocals = 0;

	if (clientData.LocalsByID == 0) return false;
	sprintf(line, "%p", hVessel);
	if ((entry = (struct IDload*)clientData.GlobalsByHandle->get(line)) == NULL) return false;
	//sprintf(msg, "STC_isVesselLocal/handle vH%s, linkage=0x%02X", line, entry->linkage);
	nbOfLocals = clientData.LocalsByID->get_count();

	for (unsigned long int bucket=0;
		bucket < clientData.LocalsByID->get_size();
		bucket++)
	{
		if ((entryHash = clientData.LocalsByID->content(bucket)) == NULL) continue;
		if ((entry = (struct IDload*)entryHash->data) == NULL) continue;
		if (entry->object == hVessel) {
			//sprintf(line, "%s = [L%d](bucket #%d), IS local, among %d locals", msg, entry->ID, bucket, nbOfLocals); tlogging(5, line);
			return true;
		}
	}
	//sprintf(line, "%s is NOT local, among %d locals", msg, nbOfLocals); tlogging(5, line);
	return false;
}

bool STC_isVesselLocal(int gID)
{
	IDload* entry;
	char line[20];
	if (clientData.LocalsByID == 0) return false;
	sprintf(line, "%d", gID);
	if ((entry = (struct IDload*)clientData.GlobalsByID->get(line)) == NULL) return false;
	return STC_isVesselLocal(entry->object);
}

void STC_UpdateState(IDload* entry)
{
	char SNDName[10] = "SND456789";
	char fileName[100];
	//char trckFile[10] = "TKS456789";
	char line[BUFFERSIZE], * quotebreak, debugMsg[LINESIZE];
	int gID = entry->ID;

	itoa(gID, SNDName + 3, 10);
	sprintf(fileName, "%s%s", STC_PATH, SNDName);
	//itoa(gID, trckFile + 3, 10);
	sprintf(debugMsg, "STC_UpdateState: updating [G%d]->%s", gID, fileName);
	//tlogging(5, debugMsg);

	//
	// Saving the current state in a local file
	// 
	// COMMON PROCESS
	FILEHANDLE stateFile=oapiOpenFile(fileName, FileAccessMode::FILE_OUT, PathRoot::MODULES);
	
	//entry->vessel->SaveDefaultState(stateFile);
	sprintf(line, "STATUS %s", entry->Landed?"Landed":"Flying");
	if (entry->Landed)
		sprintf(line, "%s %5d %12.9f %12.9f %12.9f", line, entry->LandBody, entry->LandInfo[0],entry->LandInfo[1],entry->LandInfo[2]);
	oapiWriteLine(stateFile, line);

	// OMX: EXTENDED state vector if STC_offergInProcess and current vessel being offered
	// this routine is run as "Client A" (offering client)
	if (STC_offergInProcess) if (gID == STC_globalId) {
		sprintf(debugMsg, "%s, running OFFER for \"%s\", extended status", debugMsg, STC_offrdName);
		//tlogging(5, debugMsg);
		for (int matchi = entry->vessel->GetPropellantCount() - 1; matchi >= 0; matchi--)
		{
			sprintf(line, "$XF%d %f %f %f",
				matchi,
				entry->vessel->GetPropellantEfficiency(entry->vessel->GetPropellantHandleByIndex(matchi)),
				entry->vessel->GetPropellantMaxMass(entry->vessel->GetPropellantHandleByIndex(matchi)),
				entry->vessel->GetPropellantMass(entry->vessel->GetPropellantHandleByIndex(matchi)));
			oapiWriteLine(stateFile, line);
		}
		for (int matchi = entry->vessel->GetThrusterCount() - 1; matchi >= 0; matchi--)
		{
			sprintf(line, "$XT%d %f %f",
				matchi,
				entry->vessel->GetThrusterMax0(entry->vessel->GetThrusterHandleByIndex(matchi)),
				entry->vessel->GetThrusterLevel(entry->vessel->GetThrusterHandleByIndex(matchi)));
			oapiWriteLine(stateFile, line);
		}

		// then, also creates a "(ghost)" with the vessel's state
		bool exselfCreate = selfCreate;
		VESSELSTATUS2 ghoststate;
		VESSEL* ghostV;
		OBJHANDLE entryHandle = entry->vessel->GetHandle(), ghostH;
		MATRIX3 rotation, temprot;
		VECTOR3 deltaV = unit(_V(0, 0.5, -0.866)); // -1 m/s, 30 deg top
		VECTOR3 absDeltaV;

		ghoststate = dinfo.snapshot[entry->object];
		ghoststate.version = 2; // for safety, due to DefSetStateEx later
		entry->vessel->GetRotationMatrix(temprot);
		RotateVector(deltaV, temprot, absDeltaV);
		ghoststate.rvel += absDeltaV;
		ghoststate.vrot = _V(0, 0, 0);

		if ((ghostH = oapiGetVesselByName(STC_ghostName)) == NULL) {
			selfCreate = true;
			ghostH = oapiCreateVesselEx(STC_ghostName, "OMPDefault", &ghoststate);
			STC_ghostH = ghostH;
			selfCreate = exselfCreate;
			sprintf(debugMsg, "%s, creating %s", debugMsg, STC_ghostName);
			//tlogging(5, debugMsg);
		} else sprintf(debugMsg, "%s, re-using %s", debugMsg, STC_ghostName);
		ghostV = oapiGetVesselInterface(ghostH);
		ghostV->DefSetStateEx(&ghoststate);
		if (entry->Landed)
			STC_LandIt(ghostV, ghoststate.rbody, entry->LandInfo[0], entry->LandInfo[1], entry->LandInfo[2]);

		if (entryHandle == oapiGetFocusObject()) {
			//oapiSetFocusObject(ghostH);
			oapiCameraAttach(ghostH, 1);
			sprintf(debugMsg, "%s + set focus", debugMsg);
		} // does nothing if the focus is NOT on the transferred vessel (e.g. TKBCK)
	}

	// (TODO) DELTAGLIDER-specific properties (hence, limited to DeltaGlider, at the moment)
	char* classname = "DeltaGlider";
	if (strcmp(classname, entry->vessel->GetClassNameA()) == 0) {	}
	
	// also updating animations (COMMON PROCESS)
	strcpy(line, OMPSTATELABEL);
	ANIMATION* anim;
	UINT anims = entry->vessel->GetAnimPtr(&anim);
	for (UINT i = 0; i < anims; i++)
	{   // e.g.: 9A9999999999D93F 9A9999999999D93F 0 0 0 0 E03F F03F F03F F03F 0 0 0 0 0 0
		if ((i % MAXANIMPERLINE) == 0)
		{
			oapiWriteLine(stateFile, line);
			sprintf(line, "%X", i);
			quotebreak = line + strlen(line);
		}
		//Minimum double value serializing: inverse byte order, so full numbers have leading zeros, which will be skipped
		*quotebreak = ' ';
		byte* s = (byte*)(void*)&(anim[i].state); // just creates a variable with the right size
		for (int j = 0; j < 8; j++)
		{
			if (s[j] == 0 && j < 7 && *quotebreak == ' ') continue;
			byte u = s[j] >> 4;
			if (u != 0 || *quotebreak != ' ') *(++quotebreak) = u > 9 ? 'A' + u - 10 : '0' + u;
			u = s[j] & 0x0F;
			*(++quotebreak) = u > 9 ? 'A' + u - 10 : '0' + u;
		}
		*(++quotebreak) = 0x00;
	}
	oapiWriteLine(stateFile, line);

	oapiCloseFile(stateFile, FileAccessMode::FILE_OUT);

	//
	// sending up-to-date GINFO to server via TCP
	//
	char pingbuf[BUFFERSIZE];
	char ginfo[13];
	sprintf(ginfo, "GINFO %d", gID);
	sprintf(pingbuf, "%s\"C%s\"%s", ginfo,
			entry->vessel->GetName(),
		entry->vessel->GetClassNameA());
	int matchi;
	int bufferOffset = 0;
	bool ostate = false;
	stateFile = oapiOpenFile(fileName, FileAccessMode::FILE_IN, PathRoot::MODULES);
	int sendState = 1;
	for (; sendState != 0; sendState++)
	{
		sprintf(line, "--> %s", pingbuf);
		tlogging(5, line);
		int match = (int)dinfo.shsfxasc[0]; //Pascal-Type String (First character=length)
		int buflen = strlen(pingbuf);
		//do not insert a NULL-character due to it's string terminating nature in C++
		//instead such, insert a 0x03-character (end of text)
		for (matchi = 1; matchi <= match; matchi++)
			if ((pingbuf[buflen - 1 + matchi] = dinfo.shsfxasc[matchi]) == 0x00) pingbuf[buflen - 1 + matchi] = 0x03;
		//add a string termination
		pingbuf[buflen - 1 + matchi] = 0x00;
		EnterCriticalSection(&(xthreaddata->mutex));
		// the current content of pingbuf is sent.
		if (send(xthreaddata->com, pingbuf, strlen(pingbuf), 0) == SOCKET_ERROR)
		{
			sprintf(line, "Error sending via TCP %d to server:%d", xthreaddata->com, ntohs(xthreaddata->remote_addr.sin_port));
			tlogging(20, line);
		}
		LeaveCriticalSection(&(xthreaddata->mutex));
		// if more data is to be sent, the beginning of pingbuf is re-used,
		// the content after bufferOffset is given by lines from stateFile
		if (sendState > 0)
		{
			char* oapiLine;
			if (oapiReadScenario_nextline(stateFile, oapiLine))
			{
				if (strcmp(oapiLine, OMPSTATELABEL) == 0)
				{
					ostate = true;
					sprintf(pingbuf, "%s:-", ginfo);
				}
				else if (oapiLine[0] == '$') sprintf(pingbuf, "%s:%s", ginfo, oapiLine);
				else sprintf(pingbuf, "%s:%c%s", ginfo, ostate ? '^' : '|', oapiLine);
				// pingbuf will be sent at the next "for" loop
			}
			else
			{
				oapiCloseFile(stateFile, FileAccessMode::FILE_IN);
				//CopyFile(fileName, trckFile, false); // <===== DEBUG need
				DeleteFile(fileName);
				sendState = -1; // then will become 0 at the exit of the "for" loop, hence terminating the loop (sooo, complicated!)
			}
		}
	}
	sprintf(debugMsg, "%s, sent!", debugMsg); tlogging(5, debugMsg);
}

void STC_RestoreState(IDload* entry, bool deleteAfter)
{
	// This routine is run because GINFO case '-' was received.
	// Lines "^" will be interpreted afterwards in OMPClient.cpp.
	// Hence, <fileName> shall only contain "|" and "$" lines from GINFO.
	// Lines "$" are for vessel transfer, to be interpreted by "Client B" (accepting client)
	// if B's STC_clearToAccpt == false and B's STC_globalId > 0. Then,
	// B's STC_clearToAccpt is turned true.
	char RCVName[10] = "RCV456789";
	//char trckFile[10] = "TKS456789";
	char fileName[100];
	FILEHANDLE stateFile;
	char* oapiLine;
	char line[LINESIZE], key[100];
	bool extendedStatusReceived = false;
	int nItem;
	double maxValue, currentValue, efficiency;
	VESSELSTATUS2 status = dinfo.snapshot[entry->object]; // includes status.version = 2?
	// declaring more variable for ParkingBrake status:
	VESSELSTATUS2 vs;
	bool justLanded = false, recreation = false;
	IDload* pBody;
	OBJHANDLE hPbody;
	double longitude, latitude, radius;
	double heading = 0.0;
	unsigned short gBody;

	itoa(entry->ID, RCVName + 3, 10);
	sprintf(fileName, "%s%s", STC_PATH, RCVName);
	//itoa(entry->ID, trckFile + 3, 10);
	if ((stateFile = oapiOpenFile(fileName, FileAccessMode::FILE_IN_ZEROONFAIL, PathRoot::MODULES)) == 0)
	{	// the file doesn't exist: ID is likely a local ID if just recreated from startmove
		if (!STC_clearToAccpt && entry->ID == STC_nxtLocal && entry->linkage == 1) {
			itoa(STC_globalId, RCVName + 3, 10);
			sprintf(fileName, "%s%s", STC_PATH, RCVName);
			//itoa(STC_globalId, trckFile + 3, 10);
			if ((stateFile = oapiOpenFile(fileName, FileAccessMode::FILE_IN_ZEROONFAIL, PathRoot::MODULES))
				== 0) {
				sprintf(line, "STC_RestoreState [G%d] cannot open file \"%s\"", STC_globalId, fileName);
				tlogging(20, line);
				return;
			}
		}
		else {
			sprintf(line, "STC_RestoreState [G%d] cannot open file \"%s\"", entry->ID, fileName);
			tlogging(20, line);
			return;
		}
	}
	sprintf(line, "STC_RestoreState [G/L%d], then delete? %s", entry->ID, deleteAfter?"yes":"no");
	//tlogging(5, line);
	VECTOR3 direction, location;
	location = _V(0, 0, 0);

	// Expected first line of <statefile> is STATUS
	//CopyFile(fileName, trckFile, false); // <===== DEBUG need (trckFile is saved at Orbiter's root)
	oapiReadScenario_nextline(stateFile, oapiLine);
	// e.g.
	//   STATUS Landed Earth
	// 	 POS -52.3397127 4.8146463
	// 	 HEADING 80.00
	// reformated into:
	//   STATUS Landed <gBody_ID> <Long> <Lat> <Headg>
	if (!strnicmp(oapiLine, "STATUS", 6)) {
		if ((!strnicmp(oapiLine + 7, "Landed", 6))) {
			if (!entry->Landed) justLanded = true;
			sprintf(line, "%s, Landed", line); //tlogging(5, line);
			if (sscanf(oapiLine + 14, "%d %lf %lf %lf", &gBody, &longitude, &latitude, &heading) != 4) {
				sprintf(line, "%s, format ERROR in %s", line, oapiLine + 14);
				tlogging(20, line);
				return;
			}
			sprintf(key, "%d", gBody);
			if ((pBody = (struct IDload*)clientData.GlobalsByID->get(key)) == NULL) {
				sprintf(line, "%s, unknown grav.body ID_%d", line, gBody);
				tlogging(20, line);
				return;
			}
			else hPbody = pBody->object;
		}
		else entry->Landed = false; // resets the use of SI packets
	}
	else {
		sprintf(line, "%s, %s not starting with STATUS line!", line, fileName);
		tlogging(20, line);
		return;
	}

	if (!STC_clearToAccpt && entry->ID == STC_nxtLocal && entry->linkage==1) {
		// an accepting process is running AND this vessel is the one transferred
		sprintf(line, "%s, Extended GINFO", line);
		while (oapiReadScenario_nextline(stateFile, oapiLine)) {
			if (!strnicmp(oapiLine, "$XF", 3)) {
				extendedStatusReceived = true;
				/*sprintf(line, "$XF%d %f %f %f",
					matchi,
					entry->vessel->GetPropellantEfficiency(entry->vessel->GetPropellantHandleByIndex(matchi)),
					entry->vessel->GetPropellantMaxMass(entry->vessel->GetPropellantHandleByIndex(matchi)),
					entry->vessel->GetPropellantMass(entry->vessel->GetPropellantHandleByIndex(matchi)));					*/
				if (sscanf(oapiLine + 3, "%d %lf %lf %lf", &nItem, &efficiency, &maxValue, &currentValue) != 4) {
					sprintf(line, "%s, format ERROR in Fu.#%d: eff %f, max %f current %f", line, nItem, efficiency, maxValue, currentValue);
					tlogging(20, line);
				}
				else {
					sprintf(line, "%s, %s", line, oapiLine);
				}
				entry->vessel->SetPropellantEfficiency(entry->vessel->GetPropellantHandleByIndex(nItem), efficiency);
				entry->vessel->SetPropellantMaxMass(entry->vessel->GetPropellantHandleByIndex(nItem), maxValue);
				entry->vessel->SetPropellantMass(entry->vessel->GetPropellantHandleByIndex(nItem), currentValue);
			}
			else if (!strnicmp(oapiLine, "$XT", 3)) {
				extendedStatusReceived = true;
				/*sprintf(line, "$XT%d %f %f",
					matchi,
					entry->vessel->GetThrusterMax0(entry->vessel->GetThrusterHandleByIndex(matchi)),
					entry->vessel->GetThrusterLevel(entry->vessel->GetThrusterHandleByIndex(matchi))); */
				if (sscanf(oapiLine + 3, "%d %lf %lf", &nItem, &maxValue, &currentValue) != 3) {
					sprintf(line, "%s, format ERROR in Th.#%d: max %f current %f", line, nItem, maxValue, currentValue);
					tlogging(20, line);
				}
				entry->vessel->SetThrusterMax0(entry->vessel->GetThrusterHandleByIndex(nItem), maxValue);
				entry->vessel->SetThrusterLevel(entry->vessel->GetThrusterHandleByIndex(nItem), currentValue);
			}
			else entry->vessel->ParseScenarioLineEx(oapiLine+1, &status);  // anything not recognised is passed on to Orbiter
		}
		if (extendedStatusReceived) sprintf(line, "%s (found)", line);
		else sprintf(line, "%s (NOT found!)", line);
	}
	else {
		sprintf(line, "%s, standard", line);
		// reads the rest of the GINFO (before "-")
		while (oapiReadScenario_nextline(stateFile, oapiLine)) entry->vessel->ParseScenarioLineEx(oapiLine + 1, &status);
	}

	if (justLanded) {
		STC_LandIt(entry->vessel, hPbody, longitude, latitude, heading);
		entry->Landed = true;
		entry->LandBody = gBody;
		entry->LandInfo[0] = longitude;
		entry->LandInfo[1] = latitude;
		entry->LandInfo[2] = heading;
	}

	sprintf(line, "%s, done.", line);
	tlogging(5, line);

	oapiCloseFile(stateFile, FileAccessMode::FILE_IN);
	if (deleteAfter) {
		DeleteFile(fileName);
		//sprintf(line, "STC_RestoreState: deleted %s", fileName); tlogging(5, line);
	}
	return;
}
