//////////////////////////////////////////////////////////////////////
//
// HashTable.cpp: implementation of the CHashTable class.
//
//////////////////////////////////////////////////////////////////////

#pragma unmanaged
#include "HashTable.h"
#include "HashData.h"
#include <string.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHashTable::CHashTable(unsigned short int power_of_2)
{
	HashTable=new CHashData[HashSize= HASHSIZE(power_of_2)];
	empty=true;
	count=0;
	count_inner=0;	
}

CHashTable::~CHashTable()
{	
	delete [] HashTable;
	HashTable=0;
}

//////////////////////////////////////////////////////////////////////
// HashTable functions
//////////////////////////////////////////////////////////////////////

/// <summary>
/// Adds or update a key putkey associated to newentry. If putkey already exists, the previous value is returned. If putkey is new, 0 is returned returned.
/// </summary>
/// <param name="putkey">key to be added or updated</param>
/// <param name="newentry">new value for the key</param>
/// <returns>0 if putkey was new, previous value is putkey was updated.</returns>
void *CHashTable::put(char *putkey, void *newentry)
{
	if (putkey==0) return 0;
	unsigned long int position=hash((unsigned char *) putkey, strlen(putkey), 0);
	position&=HashSize-1;
	CHashData *entry=&HashTable[position];
	empty=false;
	if (entry->key!=0)
	{
		int res;
		while ((res=strcmp(putkey, entry->key))!=0 && entry->next!=0) entry=entry->next;
		if (res==0)
		{
			void *oldentry=entry->data;
			entry->data=newentry;		
			return oldentry;
		}
		else
		{
			CHashData *addentry=new CHashData();
			addentry->key=new char[strlen(putkey)+1];
			strcpy(addentry->key,putkey);
			addentry->data=newentry;
			addentry->next=0;
			entry->next=addentry;
			count+=1;						
			return 0;
		}		
	}
	else
	{
		entry->key=new char[strlen(putkey)+1];
		strcpy(entry->key,putkey);
		entry->data=newentry;
		entry->next=0;
		count+=1;
		count_inner+=1;		
		return 0;
	}
}

void *CHashTable::get(char *getkey)
{
	if (getkey==0) return 0;
	unsigned long int position=hash((unsigned char *) getkey, strlen(getkey), 0);
	position&=HashSize-1;
	CHashData *entry=&HashTable[position];
	int res=1;
	if (entry->key!=0) while ((res=strcmp(getkey, entry->key))!=0 && entry->next!=0) entry=entry->next;
	if (res==0)	return entry->data;	else return 0;
}

void *CHashTable::del(char *getkey)
{
	if (getkey==0) return 0;
	unsigned long int position=hash((unsigned char *) getkey, strlen(getkey), 0);
	position&=HashSize-1;
	CHashData *entry=&HashTable[position];
	CHashData *nextentry=entry->next;
	void *oldentry;
	if (entry->key!=0)
	{
		if (strcmp(getkey, entry->key)==0)
		{
			oldentry=entry->data;
			delete [] entry->key;
			entry->key=0;
			if (nextentry!=0)
			{			
				entry->data=nextentry->data;
				entry->key=nextentry->key;
				entry->next=nextentry->next;
				delete nextentry;
				nextentry=0;
			}
			else
			{
				entry->data=0;
				entry->key=0;
				count_inner-=1;
			}
			count-=1;
			if (count==0) empty=true;
			return oldentry;
		}
		else if (nextentry!=0)
		{
			int res;
			while ((res=strcmp(getkey, nextentry->key))!=0 && nextentry->next!=0)
			{
				entry=nextentry;
				nextentry=nextentry->next;
			}
			if (res==0)
			{
				oldentry=nextentry->data;
				delete [] nextentry->key;
				nextentry->key=0;
				entry->next=nextentry->next;
				delete nextentry;
				nextentry=0;
				count-=1;
				if (count==0) empty=true;
				return oldentry;
			}			
		}		
	}
	return 0;
}

void *CHashTable::clear(bool startover)
{
	if (startover || last_clear_point<0 || last_clear_point>=HashSize) last_clear_point=0;
	CHashData *entry=&HashTable[last_clear_point];
	CHashData *nextentry=entry->next;
	void *oldentry=entry->data;
	unsigned long int loop_entry_point=last_clear_point;
	while (entry->key==0 && nextentry==0) 
	{
		last_clear_point+=1;
		if (last_clear_point>=HashSize) last_clear_point=0;
		if (last_clear_point==loop_entry_point) return 0;
		entry=&HashTable[last_clear_point];
		nextentry=entry->next;
		oldentry=entry->data;
	}
	delete [] entry->key;
	entry->key=0;
	if (nextentry!=0)
	{			
		entry->data=nextentry->data;
		entry->key=nextentry->key;
		entry->next=nextentry->next;
		delete nextentry;
		nextentry=0;
	}
	else
	{
		entry->data=0;
		entry->key=0;
		last_clear_point+=1;
		count_inner-=1;
	}
	count-=1;
	if (count==0) empty=true;
	return oldentry;	
}

CHashData *CHashTable::content(unsigned long int bucket)
{
	if (bucket<0 || bucket>=HashSize) return 0;
	return &HashTable[bucket];
}

unsigned long int CHashTable::get_size()
{
	return HashSize;
}

float CHashTable::get_ratio()
{
	if (count==0) return 1.0;
	unsigned long int longest_run=1, current_run;
	CHashData *entry;
	for(int i=0;i<(int)HashSize;i++)
	{
		current_run=1;
		entry=&HashTable[i];
		while (entry->next!=0)
		{
			current_run+=1;
			entry=entry->next;
		}
		if (current_run>longest_run) longest_run=current_run;
	}
	return ((float)HashSize)/((float)longest_run)*((float)count_inner)/((float)count);
}

unsigned long int CHashTable::get_count()
{
	return count;
}

//////////////////////////////////////////////////////////////////////
// Hash function
//////////////////////////////////////////////////////////////////////

/*
--------------------------------------------------------------------
mix -- mix 3 32-bit values reversibly.
For every delta with one or two bits set, and the deltas of all three
  high bits or all three low bits, whether the original value of a,b,c
  is almost all zero or is uniformly distributed,
* If mix() is run forward or backward, at least 32 bits in a,b,c
  have at least 1/4 probability of changing.
* If mix() is run forward, every bit of c will change between 1/3 and
  2/3 of the time.  (Well, 22/100 and 78/100 for some 2-bit deltas.)
mix() was built out of 36 single-cycle latency instructions in a 
  structure that could supported 2x parallelism, like so:
      a -= b; 
      a -= c; x = (c>>13);
      b -= c; a ^= x;
      b -= a; x = (a<<8);
      c -= a; b ^= x;
      c -= b; x = (b>>13);
      ...
  Unfortunately, superscalar Pentiums and Sparcs can't take advantage 
  of that parallelism.  They've also turned some of those single-cycle
  latency instructions into multi-cycle latency instructions.  Still,
  this is the fastest good hash I could find.  There were about 2^^68
  to choose from.  I only looked at a billion or so.
--------------------------------------------------------------------
*/
#define mix(a,b,c) \
{ \
  a -= b; a -= c; a ^= (c>>13); \
  b -= c; b -= a; b ^= (a<<8); \
  c -= a; c -= b; c ^= (b>>13); \
  a -= b; a -= c; a ^= (c>>12);  \
  b -= c; b -= a; b ^= (a<<16); \
  c -= a; c -= b; c ^= (b>>5); \
  a -= b; a -= c; a ^= (c>>3);  \
  b -= c; b -= a; b ^= (a<<10); \
  c -= a; c -= b; c ^= (b>>15); \
}

/*
--------------------------------------------------------------------
hash() -- hash a variable-length key into a 32-bit value
  k       : the key (the unaligned variable-length array of bytes)
  len     : the length of the key, counting by bytes
  initval : can be any 4-byte value
Returns a 32-bit value.  Every bit of the key affects every bit of
the return value.  Every 1-bit and 2-bit delta achieves avalanche.
About 6*len+35 instructions.

The best hash table sizes are powers of 2.  There is no need to do
mod a prime (mod is sooo slow!).  If you need less than 32 bits,
use a bitmask.  For example, if you need only 10 bits, do
  h = (h & hashmask(10));
In which case, the hash table should have hashsize(10) elements.

If you are hashing n strings (unsigned char **)k, do it like this:
  for (i=0, h=0; i<n; ++i) h = hash( k[i], len[i], h);

By Bob Jenkins, 1996.  bob_jenkins@burtleburtle.net.  You may use this
code any way you wish, private, educational, or commercial.  It's free.

See http://burtleburtle.net/bob/hash/evahash.html
Use for hash table lookup, or anything where one collision in 2^^32 is
acceptable.  Do NOT use for cryptographic purposes.
--------------------------------------------------------------------
*/

unsigned long  int CHashTable::hash(register unsigned char *k, register unsigned long  int  length, register unsigned long  int  initval)
//register unsigned char *k;        /* the key */
//register unsigned long  int  length;   /* the length of the key */
//register unsigned long  int  initval;  /* the previous hash, or an arbitrary value */
{
   register unsigned long  int a,b,c,len;

   /* Set up the internal state */
   len = length;
   a = b = 0x9e3779b9;  /* the golden ratio; an arbitrary value */
   c = initval;         /* the previous hash value */

   /*---------------------------------------- handle most of the key */
   while (len >= 12)
   {
      a += (k[0] +((unsigned long  int)k[1]<<8) +((unsigned long  int)k[2]<<16) +((unsigned long  int)k[3]<<24));
      b += (k[4] +((unsigned long  int)k[5]<<8) +((unsigned long  int)k[6]<<16) +((unsigned long  int)k[7]<<24));
      c += (k[8] +((unsigned long  int)k[9]<<8) +((unsigned long  int)k[10]<<16)+((unsigned long  int)k[11]<<24));
      mix(a,b,c);
      k += 12; len -= 12;
   }

   /*------------------------------------- handle the last 11 bytes */
   c += length;
   switch(len)              /* all the case statements fall through */
   {
   case 11: c+=((unsigned long  int)k[10]<<24);
   case 10: c+=((unsigned long  int)k[9]<<16);
   case 9 : c+=((unsigned long  int)k[8]<<8);
      /* the first byte of c is reserved for the length */
   case 8 : b+=((unsigned long  int)k[7]<<24);
   case 7 : b+=((unsigned long  int)k[6]<<16);
   case 6 : b+=((unsigned long  int)k[5]<<8);
   case 5 : b+=k[4];
   case 4 : a+=((unsigned long  int)k[3]<<24);
   case 3 : a+=((unsigned long  int)k[2]<<16);
   case 2 : a+=((unsigned long  int)k[1]<<8);
   case 1 : a+=k[0];
     /* case 0: nothing left to add */
   }
   mix(a,b,c);
   /*-------------------------------------------- report the result */
   return c;
}
