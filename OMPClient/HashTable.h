//////////////////////////////////////////////////////////////////////
//
// HashTable.h: interface for the CHashTable class.
//
//////////////////////////////////////////////////////////////////////

#define HASHSIZE(n) ((unsigned long  int)1<<(n))

class CHashData;
class CHashTable  
{
public:
	CHashTable(unsigned short int power_of_2);
	void *put(char *putkey, void *newentry);
	void *get(char *getkey);
	void *del(char *getkey);
	void *clear(bool startover);
	CHashData *content(unsigned long int bucket);
	unsigned long int get_size();
	float get_ratio();
	unsigned long int get_count();
	virtual ~CHashTable();
	bool empty;

private:
	CHashData *HashTable;
	unsigned long int HashSize;
	unsigned long int count_inner;
	unsigned long int count;
	unsigned long int last_clear_point;
	unsigned long int hash(register unsigned char *k, register unsigned long  int  length, register unsigned long  int  initval);

};