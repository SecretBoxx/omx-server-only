//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by OMPClient.rc
//
#define IDUPDOWN                        9
#define IDD_MYDIALOG                    101
#define IDD_MYDIALOGLPD                 102
#define IDI_CLOSE                       109
#define IDI_UP                          110
#define IDI_DOWN                        111
#define IDD_STATUS                      113
#define IDD_NETWORK                     125
#define IDD_TIME                        126
#define IDD_SHELL                       127
#define IDD_LOG                         128
#define IDD_MYDIALOGSIM                 129
#define IDB_LS0                         133
#define IDB_LCP                         135
#define IDB_LP0                         136
#define IDB_LS1                         137
#define IDB_LC0                         138
#define IDB_LC1                         139
#define IDB_LCF                         140
#define IDB_SS0                         141
#define IDB_SS1                         142
#define IDB_SSP                         143
#define IDB_SP0                         144
#define IDB_SP1                         145
#define IDB_SP2                         146
#define IDB_SC0                         147
#define IDB_SC1                         148
#define IDB_SCF                         149
#define IDB_SCP                         150
#define IDB_L                           151
#define IDB_LC                          152
#define IDB_LP                          154
#define IDB_P3                          157
#define IDB_S                           158
#define IDB_SC                          160
#define IDB_SP                          162
#define IDB_P2                          164
#define IDB_P1                          165
#define IDB_SFC                         166
#define IDB_LFC                         167
#define IDD_DIALOG1                     170
#define IDD_DIALOG2                     171
#define IDD_EMPTY                       172
#define IDC_WHOAMI                      173
#define IDC_IAM                         174
#define IDC_CLNUDP                      841
#define IDC_SRVTCP                      842
#define IDC_CLNTCP                      843
#define IDC_USERNAME                    844
#define IDC_PASSWORD                    845
#define IDC_SRVIP                       846
#define IDC_CLNIP                       847
#define IDC_SHCON                       865
#define IDC_LATMX                       913
#define IDC_LATAV                       914
#define IDC_CURLAT                      941
#define IDC_CURDEL                      942
#define IDC_WRNMJD                      943
#define IDC_WRNLAT                      944
#define IDC_WRNDEL                      945
#define IDC_ALAMJD                      946
#define IDC_ALALAT                      947
#define IDC_ALADEL                      948
#define IDC_LGLOG                       949
#define IDC_LGCLEAR                     950
#define IDC_SNTPRESET                   951
#define IDC_CURSYN                      952
#define IDC_WRNSYN                      953
#define IDC_ALASYN                      954
#define IDC_SHPFXESC                    964
#define IDC_SHSYSESC                    965
#define IDC_SMCON                       966
#define IDC_CURMJD                      967
#define IDC_SNTPSTATUS                  968
#define IDC_SNTPLOG                     969
#define IDC_SHBUF                       970
#define IDC_SHSFXASC                    971
#define IDC_SHCLEAR                     972
#define IDS_INFO                        1000
#define IDS_TYPE                        1001
#define IDC_INPUT                       1002
#define IDC_TEST                        1010
#define IDC_TAB                         1011
#define IDC_TAB2                        1012
#define IDC_TAB3                        1013
#define IDC_TAB4                        1014
#define IDC_TAB5                        1015
#define IDC_LATMN                       1016
#define IDC_MJDMX                       1017
#define IDC_MJDAV                       1018
#define IDC_MJDMN                       1019
#define IDC_SYNMX                       1020
#define IDC_SYNAV                       1021
#define IDC_SYNMN                       1022
#define IDC_SYNFR                       1025
#define IDC_DELMX                       1026
#define IDC_DELAV                       1027
#define IDC_DELMN                       1028
#define IDC_SYNFR2                      1034
#define IDC_SYNFR3                      1035
#define IDC_SYNFR4                      1036
#define IDC_SRVUDP                      1040
#define IDC_DELFR                       1041
#define IDC_DELFR2                      1042
#define IDC_DELFR3                      1043
#define IDC_DELFR4                      1044
#define IDC_LATFR                       1045
#define IDC_LATFR2                      1046
#define IDC_LATFR3                      1047
#define IDC_LATFR4                      1048
#define IDC_MJDFR                       1049
#define IDC_MJDFR2                      1050
#define IDC_MJDFR3                      1051
#define IDC_MJDFR4                      1052
#define IDC_AHNONE                      1053
#define IDC_AHSLINE                     1054
#define IDC_AHFULL                      1055
#define IDC_SFXNONE                     1056
#define IDC_SFXCR                       1057
#define IDC_SFXLF                       1058
#define IDC_SFXCRLF                     1059
#define IDC_SFXASC                      1060
#define IDC_SHPREFIX                    1061
#define IDC_LGBUFFER                    1062
#define IDC_LGLVL0                      1063
#define IDC_LGLVL1                      1064
#define IDC_LGLVL2                      1065
#define IDC_LGLVL3                      1066
#define IDC_LGLVL4                      1067
#define IDC_LGLVL5                      1068
#define IDC_LGLVL6                      1069
#define IDC_LGLVL7                      1070
#define IDC_LGLVL8                      1071
#define IDC_LGLVL9                      1072
#define IDC_SS0                         1073
#define IDC_SS1                         1074
#define IDC_SSP                         1075
#define IDC_SP0                         1076
#define IDC_SP1                         1077
#define IDC_SP2                         1078
#define IDC_SC0                         1079
#define IDC_SC1                         1080
#define IDC_SCF                         1081
#define IDC_SCP                         1082
#define IDC_LS0                         1083
#define IDC_LS1                         1084
#define IDC_LP0                         1085
#define IDC_LC0                         1086
#define IDC_LC1                         1087
#define IDC_LCF                         1088
#define IDC_LCP                         1089
#define IDC_L                           1090
#define IDC_S                           1091
#define IDC_LFC                         1093
#define IDC_SFC                         1096
#define IDC_P2                          1099
#define IDC_LP                          1101
#define IDC_LC                          1102
#define IDC_P1                          1103
#define IDC_P3                          1104
#define IDC_SP                          1105
#define IDC_SC                          1106
#define IDC_LIST                        1108
#define IDC_LISTTEXT                    1109
#define IDC_CUSTOM                      1119
#define IDC_HAIRPINNING                 1120
#define IDC_ANALYSE                     1121
#define IDC_CUSTOMSTUN                  1122

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        174
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1122
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
