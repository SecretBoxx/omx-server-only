"Name"              = 'OMP-server commands'
"Author"            = 'Kastner-Masilko Friedrich'
"Version"           = '1.1'
"About"             = 'This grammar is used to parse OMP-server commands sent by OMP-clients. Command added from v1.1'
"Case Sensitive"    = True
"Start Symbol"      = <Command>

{All}               = {Printable}+{&7F..&FF}
{String Ch}         = {All} - ["]
{String WS}       = {String Ch} - {Whitespace} - [:]
{Id Char}           = {AlphaNumeric}+[_]
NormalConst         = {String WS}+
StringConst         = '"' {String Ch}* '"'?
EscapedConst        = ':' {All}*

<Command>           ::= as <Value> <Value> <Nick>
                    |   assign <Value> <Value> <Nick>
                    |   accept <Value> <Value>
                    |   accept
                    |   chk <Check>
                    |   check <Check>
                    |   echo <Echo>
                    |   GINFO <Value><Value><Value>
                    |   GINFO <Value><Value>
                    |   ip <Value>
                    |   ip
                    |   jn <Value><Value><Value>
                    |   join <Value><Value><Value>
                    |   lv <Value><Value>
                    |   leave <Value><Value>
                    |   ls <List>
                    |   list <List>
                    |   quit
                    |   exit
                    |   RADIO EscapedConst
                    |   REQST <Value>
                    |   REQST ping <Value>
                    |   REQST ping <Value> from <Value>
                    |   t EscapedConst
                    |   talk EscapedConst
                    |   tkbck <Value>
                    |   me EscapedConst
                    |   trk <Track>
                    |   track <Track>
                    |   d <Dump>
                    |   dump <Dump>
                    |   log <Log>
                    |   mjd <MJD>
                    |   sync <Sync>
                    |   h
                    |   help
                    |   nk <Nick>
                    |   nick <Nick>
                    |   prompt <Nick>
                    |   pr <Nick>
                    |   observe <Track>
                    |   obs <Track>
                    |   offer <Value> <Value>
                    |   offer off <StandardValue>
                    |   offer off
                    |   offer
                    |   kick <Value>
                    |   kickban <Track>

<Check>             ::= f
                    |   files
                    |   s
                    |   system
                    |   t
                    |   time
                    |   v
                    |   version
                    |   vs
                    |   vessels
                    |   l <Nick>
                    |   location <Nick>
                    |   g
                    |   gbodies
                    |   <CheckValue>
                    
<Echo>              ::= on
                    |   off
                    |
                    
<List>              ::= <ListListConst>
                    |   <ListTargetConst>
                    |   <ListDetailConst>
                    |   <ListSelectValue>                    
                    |   <ListListConst> <ListTargetConst>
                    |   <ListSelectValue> <ListTargetConst>
                    |   <ListListConst> <ListDetailConst>
                    |   <ListSelectValue> <ListDetailConst>
                    |   <ListTargetConst> <ListDetailConst>
                    |   <ListListConst> <ListTargetConst> <ListDetailConst>
                    |   <ListSelectValue> <ListTargetConst> <ListDetailConst>
                    |

<Track>             ::= off
                    |   off <StandardValue>
                    |   <StandardValue>
                    |
                    
<Dump>              ::= globals
                    |   gl
                    |   connections
                    |   cn
                    |   clients
                    |   cl
                    |   log <StandardValue> <Timing>
                    |   l <StandardValue> <Timing>
                    |
                    
<Timing>            ::= last
                    |   l
                    |   <StandardValue>
                    |
                    
<Log>               ::= cut
                    |   <LogValue>
                    |

<MJD>               ::= status
                    |   st
                    |   reset <Value> <Value> <Value>
                    |   r <Value> <Value> <Value>
                    
<Sync>              ::= status
                    |   reset
                    |   data
                    
<Nick>              ::= <Value>
                    |

<StandardValue>     ::= NormalConst
                    |   StringConst
                    |   EscapedConst
                    
<CommandConst>      ::= as
                    |   accept
                    |   assign
                    |   chk
                    |   check
                    |   echo
                    |   GINFO
                    |   ip
                    |   jn
                    |   join
                    |   lv
                    |   leave
                    |   ls
                    |   list
                    |   quit
                    |   exit
                    |   RADIO
                    |   REQST
                    |   talk
                    |   me
                    |   trk
                    |   track
                    |   dump
                    |   log
                    |   mjd
                    |   sync
                    |   h
                    |   help
                    |   nk
                    |   nick
                    |   ping
                    |   from
                    |   prompt
                    |   pr
                    |   observe
                    |   obs
                    |   offer
                    |   kick
                    |   kickban
                    
<CheckConst>        ::= f
                    |   files
                    |   s
                    |   system
                    |   t
                    |   time
                    |   v
                    |   version
                    |   vs
                    |   vessels
                    |   location
                    |   g
                    |   gbodies

<ListListConst>     ::= a
                    |   all
                    |   o
                    |   own
                    |   c
                    |   current

<ListTargetConst>   ::= u
                    |   users
                    |   ob
                    |   objects

<ListDetailConst>   ::= d
                    |   detailed
                   
<ListConst>         ::= <ListListConst>
                    |   <ListTargetConst>
                    |   <ListDetailConst>        
            
<AdminConst>        ::= globals
                    |   gl
                    |   connections
                    |   cn
                    |   clients
                    |   cl
                    |   last
                    |   l
                    |   cut
                    |   status
                    |   st
                    |   reset
                    |   r
                    |   data
                    
<Value>             ::= <StandardValue>
                    |   <CommandConst>
                    |   <CheckConst>
                    |   on
                    |   off
                    |   <ListConst>
                    |   <AdminConst>

<CheckValue>        ::= <StandardValue>
                    |   <CommandConst>
                    |   on
                    |   off
                    |   <ListConst>

<ListSelectValue>   ::= <StandardValue>
                    |   <CommandConst>
                    |   <CheckConst>
                    |   on
                    |   off
                    |   <AdminConst>
                    
<LogValue>          ::= NormalConst
                    |   StringConst
                    |   EscapedConst
                    |   s
                    |   f
