namespace Orbiter.Multiplayer
{
    public enum CelestialBodyType
    {
        Star = 0,
        Planet,
        Moon,
    }

    public class CelestialBody : GlobalObject
    {
        private double mass;
        private CelestialBodyType type;
        private int index;
        private CelestialBody parent;

        public CelestialBodyType Type
        {
            get { return type; }
        }

        public double Mass
        {
            get { return mass; }
            set { mass = value; }
        }

        public CelestialBody Parent
        {
            get { return parent; }
        }

        public CelestialBody(Server server, CelestialBodyType type, int index, CelestialBody parent) : base(server)
        {
            this.type = type;
            this.index = index;
            this.parent = parent;
        }

        public override bool EvaluateInfo(params string[] strings)
        {
            return true;
        }

        public new string Name
        {
            get
            {
                return base.Name;
            }
            set
            {
                base.Name = value;
            }
        }

        public override string[] GenerateInfo()
        {
            if (parent != null) return new string[]
                  {
                    "\""+Type.ToString()[0]+Name+'"'+Mass+'"'+Parent.Name
                  };
            return new string[]
                {
                    "\""+Type.ToString()[0]+Name+'"'+Mass
                };
        }
    }
}
