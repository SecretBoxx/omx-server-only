using System;
using System.Runtime.Serialization;

namespace Orbiter.Multiplayer
{
    public class VisibilityClusterException : Exception
    {
        public VisibilityClusterException(string message) : base(message) { }
        public VisibilityClusterException(string message, Exception innerException) : base(message, innerException) { }
        public VisibilityClusterException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
