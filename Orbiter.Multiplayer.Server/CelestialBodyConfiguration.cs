﻿//Name             : Orbiter Celestial Body Configuration
//Version          : 1.0
//Author           : Kastner-Masilko Friedrich
//About            : This grammar is used to parse Orbiter celestial body configuration files.
//Case Sensitive   : 
//Start Symbol     : 

using GoldParser;

namespace Orbiter.Multiplayer
{
	public enum CelestialBodyConfigurationSymbols : int
	{
		/// <summary> (EOF) </summary>
		S_EOF             = 0,     
		/// <summary> (Error) </summary>
		S_Error           = 1,     
		/// <summary> Comment </summary>
		S_Comment         = 2,     
		/// <summary> Whitespace </summary>
		S_Whitespace      = 3,     
		/// <summary> ';' </summary>
		S_Semi            = 4,     
		/// <summary> '=' </summary>
		S_Eq              = 5,     
		/// <summary> 'BEGIN_NAVBEACON' </summary>
		S_BEGIN_NAVBEACON = 6,     
		/// <summary> 'BEGIN_OBSERVER' </summary>
		S_BEGIN_OBSERVER  = 7,     
		/// <summary> 'BEGIN_SURFBASE' </summary>
		S_BEGIN_SURFBASE  = 8,     
		/// <summary> CONTEXT </summary>
		S_CONTEXT         = 9,     
		/// <summary> DIR </summary>
		S_DIR             = 10,     
		/// <summary> 'END_NAVBEACON' </summary>
		S_END_NAVBEACON   = 11,     
		/// <summary> 'END_OBSERVER' </summary>
		S_END_OBSERVER    = 12,     
		/// <summary> 'END_SURFBASE' </summary>
		S_END_SURFBASE    = 13,     
		/// <summary> Identifier </summary>
		S_Identifier      = 14,     
		/// <summary> NewLine </summary>
		S_NewLine         = 15,     
		/// <summary> PERIOD </summary>
		S_PERIOD          = 16,     
		/// <summary> String </summary>
		S_String          = 17,     
		/// <summary> &lt;Assignment&gt; </summary>
		S_Assignment      = 18,     
		/// <summary> &lt;Base&gt; </summary>
		S_Base            = 19,     
		/// <summary> &lt;BaseBlock&gt; </summary>
		S_BaseBlock       = 20,     
		/// <summary> &lt;Bases&gt; </summary>
		S_Bases           = 21,     
		/// <summary> &lt;Beacon&gt; </summary>
		S_Beacon          = 22,     
		/// <summary> &lt;Beacons&gt; </summary>
		S_Beacons         = 23,     
		/// <summary> &lt;Break&gt; </summary>
		S_Break           = 24,     
		/// <summary> &lt;Document&gt; </summary>
		S_Document        = 25,     
		/// <summary> &lt;Id&gt; </summary>
		S_Id              = 26,     
		/// <summary> &lt;Ids&gt; </summary>
		S_Ids             = 27,     
		/// <summary> &lt;Line&gt; </summary>
		S_Line            = 28,     
		/// <summary> &lt;Lines&gt; </summary>
		S_Lines           = 29,     
		/// <summary> &lt;NavBlock&gt; </summary>
		S_NavBlock        = 30,     
		/// <summary> &lt;Observer&gt; </summary>
		S_Observer        = 31,     
		/// <summary> &lt;ObserverBlock&gt; </summary>
		S_ObserverBlock   = 32,     
		/// <summary> &lt;Observers&gt; </summary>
		S_Observers       = 33,     
		/// <summary> &lt;WL&gt; </summary>
		S_WL              = 34      
	}
	
	public enum CelestialBodyConfigurationRules : int
	{
		/// <summary> &lt;Break&gt; ::= NewLine &lt;Break&gt; </summary>
		Break_NewLine                             = 0,    
		/// <summary> &lt;Break&gt; ::= NewLine </summary>
		Break_NewLine2                            = 1,    
		/// <summary> &lt;WL&gt; ::= &lt;Break&gt; </summary>
		WL                                        = 2,    
		/// <summary> &lt;WL&gt; ::=  </summary>
		WL2                                       = 3,    
		/// <summary> &lt;Document&gt; ::= &lt;WL&gt; &lt;Lines&gt; &lt;WL&gt; </summary>
		Document                                  = 4,    
		/// <summary> &lt;Lines&gt; ::= &lt;Lines&gt; &lt;Break&gt; &lt;Line&gt; </summary>
		Lines                                     = 5,    
		/// <summary> &lt;Lines&gt; ::= &lt;Line&gt; </summary>
		Lines2                                    = 6,    
		/// <summary> &lt;Line&gt; ::= &lt;Assignment&gt; </summary>
		Line                                      = 7,    
		/// <summary> &lt;Line&gt; ::= &lt;BaseBlock&gt; </summary>
		Line2                                     = 8,    
		/// <summary> &lt;Line&gt; ::= &lt;ObserverBlock&gt; </summary>
		Line3                                     = 9,    
		/// <summary> &lt;Line&gt; ::= &lt;NavBlock&gt; </summary>
		Line4                                     = 10,    
		/// <summary> &lt;Assignment&gt; ::= &lt;Id&gt; '=' &lt;Ids&gt; </summary>
		Assignment_Eq                             = 11,    
		/// <summary> &lt;Ids&gt; ::= &lt;Id&gt; &lt;Ids&gt; </summary>
		Ids                                       = 12,    
		/// <summary> &lt;Ids&gt; ::= &lt;Id&gt; </summary>
		Ids2                                      = 13,    
		/// <summary> &lt;BaseBlock&gt; ::= 'BEGIN_SURFBASE' &lt;Break&gt; &lt;Bases&gt; &lt;Break&gt; 'END_SURFBASE' </summary>
		BaseBlock_BEGIN_SURFBASE_END_SURFBASE     = 14,    
		/// <summary> &lt;Bases&gt; ::= &lt;Bases&gt; &lt;Break&gt; &lt;Base&gt; </summary>
		Bases                                     = 15,    
		/// <summary> &lt;Bases&gt; ::= &lt;Base&gt; </summary>
		Bases2                                    = 16,    
		/// <summary> &lt;Base&gt; ::= String &lt;Id&gt; &lt;Id&gt; </summary>
		Base_String                               = 17,    
		/// <summary> &lt;Base&gt; ::= DIR &lt;Id&gt; </summary>
		Base_DIR                                  = 18,    
		/// <summary> &lt;Base&gt; ::= DIR &lt;Id&gt; PERIOD &lt;Id&gt; &lt;Id&gt; </summary>
		Base_DIR_PERIOD                           = 19,    
		/// <summary> &lt;Base&gt; ::= DIR &lt;Id&gt; CONTEXT &lt;Id&gt; </summary>
		Base_DIR_CONTEXT                          = 20,    
		/// <summary> &lt;Base&gt; ::= DIR &lt;Id&gt; PERIOD &lt;Id&gt; &lt;Id&gt; CONTEXT &lt;Id&gt; </summary>
		Base_DIR_PERIOD_CONTEXT                   = 21,    
		/// <summary> &lt;Base&gt; ::= DIR &lt;Id&gt; CONTEXT &lt;Id&gt; PERIOD &lt;Id&gt; &lt;Id&gt; </summary>
		Base_DIR_CONTEXT_PERIOD                   = 22,    
		/// <summary> &lt;ObserverBlock&gt; ::= 'BEGIN_OBSERVER' &lt;Break&gt; &lt;Observers&gt; &lt;Break&gt; 'END_OBSERVER' </summary>
		ObserverBlock_BEGIN_OBSERVER_END_OBSERVER = 23,    
		/// <summary> &lt;Observers&gt; ::= &lt;Observers&gt; &lt;Break&gt; &lt;Observer&gt; </summary>
		Observers                                 = 24,    
		/// <summary> &lt;Observers&gt; ::= &lt;Observer&gt; </summary>
		Observers2                                = 25,    
		/// <summary> &lt;Observer&gt; ::= String String &lt;Id&gt; &lt;Id&gt; &lt;Id&gt; </summary>
		Observer_String_String                    = 26,    
		/// <summary> &lt;NavBlock&gt; ::= 'BEGIN_NAVBEACON' &lt;Break&gt; &lt;Beacons&gt; &lt;Break&gt; 'END_NAVBEACON' </summary>
		NavBlock_BEGIN_NAVBEACON_END_NAVBEACON    = 27,    
		/// <summary> &lt;Beacons&gt; ::= &lt;Beacons&gt; &lt;Break&gt; &lt;Beacon&gt; </summary>
		Beacons                                   = 28,    
		/// <summary> &lt;Beacons&gt; ::= &lt;Beacon&gt; </summary>
		Beacons2                                  = 29,    
		/// <summary> &lt;Beacon&gt; ::= &lt;Id&gt; &lt;Id&gt; &lt;Id&gt; &lt;Id&gt; &lt;Id&gt; &lt;Id&gt; </summary>
		Beacon                                    = 30,    
		/// <summary> &lt;Beacon&gt; ::= &lt;Id&gt; &lt;Id&gt; &lt;Id&gt; &lt;Id&gt; &lt;Id&gt; </summary>
		Beacon2                                   = 31,    
		/// <summary> &lt;Id&gt; ::= Identifier </summary>
		Id_Identifier                             = 32,    
		/// <summary> &lt;Id&gt; ::= 'BEGIN_SURFBASE' </summary>
		Id_BEGIN_SURFBASE                         = 33,    
		/// <summary> &lt;Id&gt; ::= 'END_SURFBASE' </summary>
		Id_END_SURFBASE                           = 34,    
		/// <summary> &lt;Id&gt; ::= 'BEGIN_OBSERVER' </summary>
		Id_BEGIN_OBSERVER                         = 35,    
		/// <summary> &lt;Id&gt; ::= 'END_OBSERVER' </summary>
		Id_END_OBSERVER                           = 36,    
		/// <summary> &lt;Id&gt; ::= 'BEGIN_NAVBEACON' </summary>
		Id_BEGIN_NAVBEACON                        = 37,    
		/// <summary> &lt;Id&gt; ::= 'END_NAVBEACON' </summary>
		Id_END_NAVBEACON                          = 38,    
		/// <summary> &lt;Id&gt; ::= DIR </summary>
		Id_DIR                                    = 39,    
		/// <summary> &lt;Id&gt; ::= PERIOD </summary>
		Id_PERIOD                                 = 40,    
		/// <summary> &lt;Id&gt; ::= CONTEXT </summary>
		Id_CONTEXT                                = 41     
	}
	
	public class CelestialBodyConfigurationNode : SyntaxNode
	{
		public CelestialBodyConfigurationNode(Rule rule, SyntaxNode[] nodes) : base(rule, nodes) {}
		
		public CelestialBodyConfigurationNode(Symbol symbol, string content) : base(symbol, content) {}
		
		public new CelestialBodyConfigurationSymbols SymbolIndex
		{
			get
			{
				return (CelestialBodyConfigurationSymbols) base.SymbolIndex;
			}
		}
		
		public new CelestialBodyConfigurationRules RuleIndex
		{
			get
			{
				return (CelestialBodyConfigurationRules) base.RuleIndex;
			}
		}
		
		public new CelestialBodyConfigurationNode this[int index]
		{
			get
			{
				return (CelestialBodyConfigurationNode) base[index];
			}
		}
		
		public new CelestialBodyConfigurationNode Trimmed
		{
			get
			{
				return (CelestialBodyConfigurationNode) base.Trimmed;
			}
		}
	}
}
