using System;

namespace Orbiter.Multiplayer.Client
{
    public enum ClockVisibility
    {
        Hidden,
        Fading,
        Visible,
    }

    public class Database
    {
        private ClockVisibility clockVisibility = ClockVisibility.Visible;
        private bool clockPulseVisible;
        private int skewMax;
        private int skewMin;
        private int skew;
        private bool freezeStatusData;
        private int mjdMax;
        private int mjdMin;
        private int mjd;
        private bool systemVisible;
        private int pulse;

        public event EventHandler StatusDataChanged;

        protected virtual void OnStatusDataChanged(EventArgs e)
        {
            if (freezeStatusData) return;
            if (StatusDataChanged != null) StatusDataChanged(this, e);
        }


        public ClockVisibility ClockVisibility
        {
            get { return clockVisibility; }
            set
            {
                clockVisibility = value;
                OnStatusDataChanged(EventArgs.Empty);
            }
        }

        public bool ClockPulseVisible
        {
            get { return clockPulseVisible; }
            set
            {
                clockPulseVisible = value;
                OnStatusDataChanged(EventArgs.Empty);
            }
        }

        public int SkewMax
        {
            get { return skewMax; }
            set
            {
                skewMax = value;
                OnStatusDataChanged(EventArgs.Empty);
            }
        }


        public int SkewMin
        {
            get { return skewMin; }
            set
            {
                skewMin = value;
                OnStatusDataChanged(EventArgs.Empty);
            }
        }

        public int Skew
        {
            get { return skew; }
            set
            {
                skew = value;
                OnStatusDataChanged(EventArgs.Empty);
            }
        }

        public int MjdMax
        {
            get { return mjdMax; }
            set
            {
                mjdMax = value;
                OnStatusDataChanged(EventArgs.Empty);
            }
        }

        public int MjdMin
        {
            get { return mjdMin; }
            set
            {
                mjdMin = value;
                OnStatusDataChanged(EventArgs.Empty);
            }
        }

        public int Mjd
        {
            get { return mjd; }
            set
            {
                mjd = value;
                OnStatusDataChanged(EventArgs.Empty);
            }
        }

        public bool SystemVisible
        {
            get { return systemVisible; }
            set
            {
                systemVisible = value;
                OnStatusDataChanged(EventArgs.Empty);
            }
        }

        public int Pulse
        {
            get { return pulse; }
            set
            {
                pulse = value;
                if (pulse > 3) pulse = 1;
                OnStatusDataChanged(EventArgs.Empty);
            }
        }

        public void FreezeStatusData()
        {
            freezeStatusData = true;
        }

        public void ThawStatusData()
        {
            if (freezeStatusData)
            {
                freezeStatusData = false;
                OnStatusDataChanged(EventArgs.Empty);
            }
        }
    }
}
