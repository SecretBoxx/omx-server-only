namespace Orbiter.Multiplayer.Client
{
    partial class ClientControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientControl));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tPStatus = new System.Windows.Forms.TabPage();
            this.statusControl1 = new Orbiter.Multiplayer.Client.StatusControl();
            this.tPNetwork = new System.Windows.Forms.TabPage();
            this.networkControl1 = new Orbiter.Multiplayer.Client.NetworkControl();
            this.tPTiming = new System.Windows.Forms.TabPage();
            this.timeControl1 = new Orbiter.Multiplayer.Client.TimeControl();
            this.Shell = new System.Windows.Forms.TabPage();
            this.shellControl1 = new Orbiter.Multiplayer.Client.ShellControl();
            this.tPLogging = new System.Windows.Forms.TabPage();
            this.logControl1 = new Orbiter.Multiplayer.Client.LogControl();
            this.tabControl1.SuspendLayout();
            this.tPStatus.SuspendLayout();
            this.tPNetwork.SuspendLayout();
            this.tPTiming.SuspendLayout();
            this.Shell.SuspendLayout();
            this.tPLogging.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(0, 234);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(360, 20);
            this.textBox1.TabIndex = 2;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(0, 256);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.richTextBox1.Size = new System.Drawing.Size(360, 24);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tPStatus);
            this.tabControl1.Controls.Add(this.tPNetwork);
            this.tabControl1.Controls.Add(this.tPTiming);
            this.tabControl1.Controls.Add(this.Shell);
            this.tabControl1.Controls.Add(this.tPLogging);
            this.tabControl1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(0, 0);
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(360, 235);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tabControl1.TabIndex = 0;
            // 
            // tPStatus
            // 
            this.tPStatus.Controls.Add(this.statusControl1);
            this.tPStatus.Location = new System.Drawing.Point(4, 25);
            this.tPStatus.Name = "tPStatus";
            this.tPStatus.Size = new System.Drawing.Size(352, 206);
            this.tPStatus.TabIndex = 0;
            this.tPStatus.Text = "Status";
            this.tPStatus.UseVisualStyleBackColor = true;
            // 
            // statusControl1
            // 
            this.statusControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statusControl1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusControl1.Location = new System.Drawing.Point(0, 0);
            this.statusControl1.Margin = new System.Windows.Forms.Padding(4);
            this.statusControl1.Name = "statusControl1";
            this.statusControl1.Size = new System.Drawing.Size(352, 206);
            this.statusControl1.TabIndex = 0;
            // 
            // tPNetwork
            // 
            this.tPNetwork.Controls.Add(this.networkControl1);
            this.tPNetwork.Location = new System.Drawing.Point(4, 22);
            this.tPNetwork.Name = "tPNetwork";
            this.tPNetwork.Size = new System.Drawing.Size(352, 209);
            this.tPNetwork.TabIndex = 1;
            this.tPNetwork.Text = "Network";
            this.tPNetwork.UseVisualStyleBackColor = true;
            // 
            // networkControl1
            // 
            this.networkControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.networkControl1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.networkControl1.Location = new System.Drawing.Point(0, 0);
            this.networkControl1.Margin = new System.Windows.Forms.Padding(4);
            this.networkControl1.Name = "networkControl1";
            this.networkControl1.Size = new System.Drawing.Size(352, 209);
            this.networkControl1.TabIndex = 0;
            // 
            // tPTiming
            // 
            this.tPTiming.Controls.Add(this.timeControl1);
            this.tPTiming.Location = new System.Drawing.Point(4, 22);
            this.tPTiming.Name = "tPTiming";
            this.tPTiming.Size = new System.Drawing.Size(352, 209);
            this.tPTiming.TabIndex = 2;
            this.tPTiming.Text = "Timing";
            this.tPTiming.UseVisualStyleBackColor = true;
            // 
            // timeControl1
            // 
            this.timeControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.timeControl1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeControl1.Location = new System.Drawing.Point(0, 0);
            this.timeControl1.Margin = new System.Windows.Forms.Padding(4);
            this.timeControl1.Name = "timeControl1";
            this.timeControl1.Size = new System.Drawing.Size(352, 209);
            this.timeControl1.TabIndex = 0;
            // 
            // Shell
            // 
            this.Shell.Controls.Add(this.shellControl1);
            this.Shell.Location = new System.Drawing.Point(4, 22);
            this.Shell.Name = "Shell";
            this.Shell.Size = new System.Drawing.Size(352, 209);
            this.Shell.TabIndex = 3;
            this.Shell.Text = "Shell";
            this.Shell.UseVisualStyleBackColor = true;
            // 
            // shellControl1
            // 
            this.shellControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.shellControl1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.shellControl1.Location = new System.Drawing.Point(0, 0);
            this.shellControl1.Margin = new System.Windows.Forms.Padding(4);
            this.shellControl1.Name = "shellControl1";
            this.shellControl1.Size = new System.Drawing.Size(352, 209);
            this.shellControl1.TabIndex = 0;
            // 
            // tPLogging
            // 
            this.tPLogging.Controls.Add(this.logControl1);
            this.tPLogging.Location = new System.Drawing.Point(4, 22);
            this.tPLogging.Name = "tPLogging";
            this.tPLogging.Size = new System.Drawing.Size(352, 209);
            this.tPLogging.TabIndex = 4;
            this.tPLogging.Text = "Logging";
            this.tPLogging.UseVisualStyleBackColor = true;
            // 
            // logControl1
            // 
            this.logControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logControl1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logControl1.Location = new System.Drawing.Point(0, 0);
            this.logControl1.Margin = new System.Windows.Forms.Padding(4);
            this.logControl1.Name = "logControl1";
            this.logControl1.Size = new System.Drawing.Size(352, 209);
            this.logControl1.TabIndex = 0;
            // 
            // ClientControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.textBox1);
            this.MinimumSize = new System.Drawing.Size(360, 280);
            this.Name = "ClientControl";
            this.Size = new System.Drawing.Size(360, 280);
            this.tabControl1.ResumeLayout(false);
            this.tPStatus.ResumeLayout(false);
            this.tPNetwork.ResumeLayout(false);
            this.tPTiming.ResumeLayout(false);
            this.Shell.ResumeLayout(false);
            this.tPLogging.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tPStatus;
        private System.Windows.Forms.TabPage tPNetwork;
        private System.Windows.Forms.TabPage tPTiming;
        private System.Windows.Forms.TabPage Shell;
        private System.Windows.Forms.TabPage tPLogging;
        private StatusControl statusControl1;
        private NetworkControl networkControl1;
        private TimeControl timeControl1;
        private ShellControl shellControl1;
        private LogControl logControl1;


    }
}
