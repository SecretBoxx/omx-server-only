using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Orbiter.Multiplayer.Client
{
    public partial class ClientControl : UserControl
    {
        [StructLayout(LayoutKind.Sequential)]
        private class Rectangle
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;

            public Size Size
            {
                get
                {
                    return new Size(Right - Left, Bottom - Top);
                }
            }
        }

        public ClientControl()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x214: //WM_SIZING
                    Rectangle rect = new Rectangle();
                    Marshal.PtrToStructure(m.LParam, rect);
                    Size = rect.Size;
                    break;
            }
            base.WndProc(ref m);
        }

        public void SetDatabase(Database database)
        {
            statusControl1.SetDatabase(database);
        }

        public void Init(bool simulation)
        {
            tabControl1.Alignment = simulation ? TabAlignment.Bottom : TabAlignment.Top;
            statusControl1.Init(simulation);
        }

    }
}
