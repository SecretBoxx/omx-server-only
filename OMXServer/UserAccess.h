﻿#pragma once

#define PASSWORDCHECKSUM "../Data/passwordChecksums"
#define OWNERSFILE "../Data/owners"
#define STCsFILE "../Data/owners"

#ifdef __cplusplus
extern "C" {
#endif

    //__declspec(dllexport) int bidule(int a, int b);
    __declspec(dllexport) int checkPassword(char* usernamePtr, int usernameLength, char* passwordPtr, int passwordLength);

#ifdef __cplusplus
}
#endif
